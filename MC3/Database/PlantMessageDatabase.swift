//
//  PlantMessageDatabase.swift
//  MC3
//
//  Created by Gianna Stylianou on 4/3/20.
//  Copyright © 2020 Gianna Stylianou. All rights reserved.
//

import Foundation

struct PlantMessage {
    var text: String = " "
}

let plantMessage1 = PlantMessage(text: "Even Essential thoughts need to be let go of once it’s time. Don’t forget to declutter your mind of them as well.")
let plantMessage2 = PlantMessage(text: "“The ability to simplify means to eliminate the unnecessary so that the necessary may speak.” - Hans Hofmann ")
let plantMessage3 = PlantMessage(text: "The goal is not only to visualise your thoughts but also to reflect on them. Take the time to understand how you feel about them and why.")
let plantMessage4 = PlantMessage(text: "“Minimalism is the intentional promotion of the things we most value and the removal of anything that distracts us from it.”  - Joshua Becker")
let plantMessage5 = PlantMessage(text: "Non-Essential thoughts weight you down more than the Essential ones. Try to get rid of them as soon as possible.")
let plantMessage6 = PlantMessage(text: "When choosing if a thought is Essential or Non-Essential ask yourself: is this thought useful or helpful? Is it just draining my energy? And them make your decision.")
let plantMessage7 = PlantMessage(text: "Decluttering your mind is useful to improve your focus and your productivity.")
let plantMessage8 = PlantMessage(text: "A Non-Essential Thought can become Essential and vice versa. Tap on Edit to modify it.")
let plantMessage9 = PlantMessage(text: "“Keep only those things that speak to your heart. Then take the plunge and discard all the rest. By doing this, you can reset your life and embark on a new lifestyle.” ― Marie Kondo")

let plantArray = [plantMessage1, plantMessage2, plantMessage3, plantMessage4, plantMessage5, plantMessage6, plantMessage7, plantMessage8, plantMessage9]

var plantCounter: Int = 0
